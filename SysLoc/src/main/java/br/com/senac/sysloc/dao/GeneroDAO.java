package br.com.senac.sysloc.dao;

import br.com.senac.sysloc.model.Genero;

public class GeneroDAO extends DAO<Genero> {

    public GeneroDAO() {
        super(Genero.class);
    }

}
