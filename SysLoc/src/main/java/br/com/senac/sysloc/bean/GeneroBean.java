package br.com.senac.sysloc.bean;

import br.com.senac.sysloc.dao.GeneroDAO;
import br.com.senac.sysloc.model.Genero;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "generoBean")
@ViewScoped
public class GeneroBean extends Bean {

    private Genero genero;

    public GeneroBean() {
        this.novo();
    }

    public void salvar() {

        GeneroDAO dao = new GeneroDAO();

        try {
            if (this.genero.getCodigo() == 0) {
                dao.save(genero);
                this.showMessageInfo("Salvo com sucesso");
            } else {
                dao.update(genero);
                this.showMessageInfo("Alterado com sucesso");
            }

        } catch (Exception ex) {
            this.showMessageErro("Falha ao Salvar");
        }

    }

    public void novo() {
        this.genero = new Genero();
    }

    public String getCodigo() {
        return genero.getCodigo() == 0 ? "" : String.valueOf(genero.getCodigo());
    }

    public void setCodigo(String id) {
        if (id.equals("")) {
            this.genero.setCodigo(0);
        } else {
            this.genero.setCodigo(Integer.parseInt(id));
        }

    }

    public String getDescricao() {
        return this.genero.getDescricao();
    }

    public void setDescricao(String descricao) {
        this.genero.setDescricao(descricao);
    }

}
