package br.com.senac.sysloc.dao;

import br.com.senac.sysloc.model.Diretor;

public class DiretorDAO extends DAO<Diretor> {

    public DiretorDAO() {
        super(Diretor.class);
    }

}
