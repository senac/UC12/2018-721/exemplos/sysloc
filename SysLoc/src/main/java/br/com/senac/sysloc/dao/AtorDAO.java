package br.com.senac.sysloc.dao;

import br.com.senac.sysloc.model.Ator;

public class AtorDAO extends DAO<Ator> {

    public AtorDAO() {
        super(Ator.class);
    }

}
