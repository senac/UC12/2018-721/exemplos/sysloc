package br.com.senac.sysloc.bean;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class Bean implements Serializable {

    protected void showMessageInfo(String msg) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null,
                new FacesMessage(
                        FacesMessage.SEVERITY_INFO,
                        msg, null));

    }

    protected void showMessageWarning(String msg) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null,
                new FacesMessage(
                        FacesMessage.SEVERITY_WARN,
                        msg, null));

    }

    protected void showMessageErro(String msg) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null,
                new FacesMessage(
                        FacesMessage.SEVERITY_ERROR,
                        msg, null));

    }

}
