package br.com.senac.sysloc.servlet;

import br.com.senac.sysloc.dao.UsuarioDAO;
import br.com.senac.sysloc.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {

        String user = request.getParameter("user");
        String password = request.getParameter("password");

        UsuarioDAO dao = new UsuarioDAO();

        Usuario usuario = dao.findByUsuario(user);

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginServlet</title>");
            out.println("</head>");
            out.println("<body>");

            if (usuario != null) {

                if (usuario.getSenha().equals(password)) {
                    // seja bem vindo 

                    out.println("<h1>Seja Bem-vindo " + usuario.getNome() + "</h1>");

                } else {
                    out.println("Erro senha incorreta");
                }

            } else {
                out.println("usuario nao existe...");
            }

            out.println("</body>");
            out.println("</html>");
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
