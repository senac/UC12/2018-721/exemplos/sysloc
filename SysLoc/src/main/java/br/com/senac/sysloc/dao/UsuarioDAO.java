package br.com.senac.sysloc.dao;

import br.com.senac.sysloc.model.Usuario;
import javax.persistence.NoResultException;
import javax.persistence.Query;

public class UsuarioDAO extends DAO<Usuario> {

    public UsuarioDAO() {
        super(Usuario.class);
    }

    public Usuario findByUsuario(String usuario) {
        Usuario u = null;

        try {
            // abrir conexao
            this.em = JPAUtil.getEntityManager();
            em.getTransaction().begin();
            //Criar query 
            Query query = em.createQuery("from Usuario u where u.usuario = :user ");// JPQL NAO É SQL
            query.setParameter("user", usuario);
            // executar 
            u = (Usuario) query.getSingleResult();
            // fechar conexao
            em.getTransaction().commit();
        } catch (NoResultException ex) {
            return null;
        } finally {
            em.close();
        }

        return u;
    }

}
