package br.com.senac.sysloc.dao;

import br.com.senac.sysloc.model.Ator;
import br.com.senac.sysloc.model.Diretor;
import br.com.senac.sysloc.model.Filme;
import br.com.senac.sysloc.model.Genero;
import br.com.senac.sysloc.model.Usuario;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

    private static EntityManagerFactory emf
            = Persistence.createEntityManagerFactory("SysLocPU");

    public static EntityManager getEntityManager() {
        try {
            return emf.createEntityManager();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {

        UsuarioDAO dao = new UsuarioDAO();
        
        Usuario usuario = dao.findByUsuario("ds") ; 
        
        System.out.println(usuario);

        
        
        
        System.exit(0);
    }

}
