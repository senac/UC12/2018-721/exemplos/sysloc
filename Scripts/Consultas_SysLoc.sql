-- 1Liste todos os gêneros
SELECT * FROM GENERO ; 
-- 2Liste todos os atores
SELECT * FROM ATOR ; 
-- 3Liste todos os filmes ordenados pelo ano
SELECT * FROM FILME ORDER BY ANO_LANCAMENTO ;
-- 4Liste todos os diretores
SELECT * FROM DIRETOR;
-- 5Liste todos os filmes com o genero (id , titulo , genero , ano)
SELECT F.ID , F.TITULO , G.NOME AS GENERO , ANO_LANCAMENTO AS ANO 
FROM FILME F
INNER JOIN GENERO G ON  F.ID_GENERO = G.ID ; 
-- 6Liste todos os filmes do genero drama
SELECT *
FROM FILME F
INNER JOIN GENERO G ON  F.ID_GENERO = G.ID
WHERE G.NOME = 'DRAMA' ; 
-- 7Liste a quantidade de filmes por genero (genero , quantidade)
SELECT G.NOME ,  COUNT(1) AS quantidade
FROM FILME F
INNER JOIN GENERO G ON  F.ID_GENERO = G.ID 
GROUP BY G.NOME; 
-- 8Liste os diretores com a quantidade de filmes que participou (nome , quantidade)

SELECT CONCAT ( D.PRIMEIRO_NOME , ' ' , D.ULTIMO_NOME) AS DIRETOR , COUNT(1) AS QUANTIDADE
FROM FILME F 
INNER JOIN DIRETOR D ON F.ID_DIRETOR = D.ID
GROUP BY ID_DIRETOR ;
-- 9Liste os filmes com a quantidade de atores ordenado pela quantidade de atores descendente (id , titulo , quantidade de atores)
SELECT F.ID , F.TITULO AS FILME , COUNT(1) AS 'QUANTIDADE DE ATORES' FROM FILME F 
INNER JOIN FILME_ATOR FA ON F.ID = FA.ID_FILME
INNER JOIN ATOR A ON FA.ID_ATOR = A.ID 
GROUP BY F.ID
;
-- 10Liste o filme mais antigo
SELECT * FROM FILME  
WHERE ANO_LANCAMENTO = (SELECT MIN(F.ANO_LANCAMENTO) FROM FILME F ) ; 

-- 11Liste o ator que mais teve participações em filmes

SELECT A.ID ,  CONCAT(A.PRIMEIRO_NOME , ' ' , A.ULTIMO_NOME ) AS ATOR  , COUNT(1) AS PARTICIPACOES
FROM ATOR A 
INNER JOIN FILME_ATOR FA 
ON A.ID = FA.ID_ATOR
INNER JOIN FILME F 
ON FA.ID_FILME = F.ID
GROUP BY A.ID HAVING COUNT(1) =  ( SELECT MAX(PARTICIPACOES ) FROM (
		SELECT A.ID ,  CONCAT(A.PRIMEIRO_NOME , ' ' , A.ULTIMO_NOME ) AS ATOR  , COUNT(1) AS PARTICIPACOES
		FROM ATOR A 
		INNER JOIN FILME_ATOR FA 
		ON A.ID = FA.ID_ATOR
		INNER JOIN FILME F 
		ON FA.ID_FILME = F.ID
		GROUP BY A.ID 
) P ); 



-- 12Liste o diretor que mais teve participações em filmes

-- 13Liste os filmes ordenados pelas avaliações
SELECT * FROM FILME ORDER BY AVALIACAO DESC ;
-- 14Liste o atores junto com a quantidade de participações em filmes

-- 15liste o ator que mais participou de filmes

-- 16liste o diretor que menos participou de filmes

-- 17liste somente os filmes com mais de 2 atores

-- 18Liste todos os filmes que tem atores com o nome de Ford

-- 19Liste todos os filmes com o genero parcial de 'Pol'
SELECT * FROM FILME F 
INNER JOIN GENERO G 
ON F.ID_GENERO =  G.ID
WHERE G.NOME LIKE 'Pol%'

-- 20Liste todos os filmes junto com os atores com o titulo de indiana ordenados pelo ano inverso


SELECT * FROM FILME_ATOR;