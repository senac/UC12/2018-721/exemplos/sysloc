-- Inserções em Genero 
-- Insira 5 gêneros: Aventura , Terror , Ficção , Drama , Romance
INSERT INTO GENERO (NOME) VALUES ('Aventura'); 
INSERT INTO GENERO (NOME) VALUES ('Terror'); 
INSERT INTO GENERO (NOME) VALUES ('Ficção') ; 
INSERT INTO GENERO (NOME) VALUES ('Drama') ; 
INSERT INTO GENERO (NOME) VALUES ('Romance') ; 

-- Inserções em Atores
-- Insira 10 atores quais queres
-- DESC ATOR ;
-- Truncate TABLE ATOR ; 
INSERT INTO ATOR (Primeiro_Nome , Ultimo_Nome , Local_Nascimento , Aniversario) VALUES ('Jonas' , 'Silva' , 'São Paulo' , '2000-01-01') ; 
INSERT INTO ATOR (Primeiro_Nome , Ultimo_Nome , Local_Nascimento , Aniversario) VALUES ('Miguel' , 'Batista' , 'Rio de Janeiro' , '1998-02-19') ; 
INSERT INTO ATOR (Primeiro_Nome , Ultimo_Nome , Local_Nascimento , Aniversario) VALUES ('Daniel' , 'Araujo' , 'Minas Gerais' , '1950-08-23') ; 
INSERT INTO ATOR (Primeiro_Nome , Ultimo_Nome , Local_Nascimento , Aniversario) VALUES ('José' , 'Perin' , 'Sergipe' , '1968-03-18') ; 
INSERT INTO ATOR (Primeiro_Nome , Ultimo_Nome , Local_Nascimento , Aniversario) VALUES ('Marco Antônio' , 'Batista' , 'Vitória' , '1970-12-02') ; 
INSERT INTO ATOR (Primeiro_Nome , Ultimo_Nome , Local_Nascimento , Aniversario) VALUES ('Isaias' , 'Soares' , 'São Paulo' , '1983-02-05') ; 
INSERT INTO ATOR (Primeiro_Nome , Ultimo_Nome , Local_Nascimento , Aniversario) VALUES ('Maurício' , 'Santos' , 'Rio de Janeiro' , '1978-07-06') ; 
INSERT INTO ATOR (Primeiro_Nome , Ultimo_Nome , Local_Nascimento , Aniversario) VALUES ('Sofia' , 'Oliveira' , 'Espírito Santo' , '2005-06-14') ; 
INSERT INTO ATOR (Primeiro_Nome , Ultimo_Nome , Local_Nascimento , Aniversario) VALUES ('Andréia' , 'Araujo' , 'Bahia' , '2003-12-30') ; 
INSERT INTO ATOR (Primeiro_Nome , Ultimo_Nome , Local_Nascimento , Aniversario) VALUES ('Valeska' , 'da Silva' , 'Amazonas' , '1991-02-19') ; 

-- SELECT * FROM ATOR;
-- Inserindo diretores 
-- Insira 5 Diretores quais queres
INSERT INTO DIRETOR (Primeiro_Nome , Ultimo_Nome , Local_Nascimento , Aniversario) VALUES ('Alex' , 'Soares' , 'Goias' , '1948-07-06') ; 
INSERT INTO DIRETOR (Primeiro_Nome , Ultimo_Nome , Local_Nascimento , Aniversario) VALUES ('Silas' , 'Gomes' , 'Santos' , '1988-06-03') ; 
INSERT INTO DIRETOR (Primeiro_Nome , Ultimo_Nome , Local_Nascimento , Aniversario) VALUES ('Jhonny' , 'Ratacaso' , 'Serra' , '1980-05-05') ; 
INSERT INTO DIRETOR (Primeiro_Nome , Ultimo_Nome , Local_Nascimento , Aniversario) VALUES ('David' , 'Martinelli' , 'Manaus' , '1987-01-21') ; 
INSERT INTO DIRETOR (Primeiro_Nome , Ultimo_Nome , Local_Nascimento , Aniversario) VALUES ('Guilherme' , 'Soares' , 'Iconha' , '1960-11-03') ; 
INSERT INTO DIRETOR (Primeiro_Nome , Ultimo_Nome , Local_Nascimento , Aniversario) VALUES ('Bruno' , 'Santos' , 'Espírito Santo' , '1982-08-05') ; 

-- Inserindo Filmes 
-- Insira 10 filmes quais queres.
-- select * from filme;
INSERT INTO filme (ID, Titulo, ANO_LANCAMENTO, AVALIACAO, TEMPO_DURACAO, ID_GENERO, ID_DIRETOR) VALUES ('1', 'Meu Pé Esquerdo', '1989', '9', '120', '2', '1');
INSERT INTO filme (ID, Titulo, ANO_LANCAMENTO, AVALIACAO, TEMPO_DURACAO, ID_GENERO, ID_DIRETOR) VALUES ('2', 'Basquete Blues', '1994', '8', '145', '1', '2');
INSERT INTO filme (ID, Titulo, ANO_LANCAMENTO, AVALIACAO, TEMPO_DURACAO, ID_GENERO, ID_DIRETOR) VALUES ('3', 'O Labirinto do Fauno', '2006', '10', '120', '1', '3');
INSERT INTO filme (ID, Titulo, ANO_LANCAMENTO, AVALIACAO, TEMPO_DURACAO, ID_GENERO, ID_DIRETOR) VALUES ('4', 'Moonlight: Sob a Luz do Luar', '2016', '8', '120', '3', '4');
INSERT INTO filme (ID, Titulo, ANO_LANCAMENTO, AVALIACAO, TEMPO_DURACAO, ID_GENERO, ID_DIRETOR) VALUES ('5', 'Dançando na Chuva', '1952', '10', '84', '4', '5');
INSERT INTO filme (ID, Titulo, ANO_LANCAMENTO, AVALIACAO, TEMPO_DURACAO, ID_GENERO, ID_DIRETOR) VALUES ('6', 'A Fraternidade é Vermelha', '1994', '9', '85', '5', '2');
INSERT INTO filme (ID, Titulo, ANO_LANCAMENTO, AVALIACAO, TEMPO_DURACAO, ID_GENERO, ID_DIRETOR) VALUES ('7', 'Boyhood: Da Infância à Juventude', '2014', '8', '120', '4', '1');
INSERT INTO filme (ID, Titulo, ANO_LANCAMENTO, AVALIACAO, TEMPO_DURACAO, ID_GENERO, ID_DIRETOR) VALUES ('8', 'O Poderoso Chefão', '1972', '10', '120', '4', '1');
INSERT INTO filme (ID, Titulo, ANO_LANCAMENTO, AVALIACAO, TEMPO_DURACAO, ID_GENERO, ID_DIRETOR) VALUES ('9', 'Cidadão Kane', '1941', '10', '120', '4', '3');
INSERT INTO filme (ID, Titulo, ANO_LANCAMENTO, AVALIACAO, TEMPO_DURACAO, ID_GENERO, ID_DIRETOR) VALUES ('10', 'Boyhood: Da Infância à Juventude', '1981', '10', '120', '4', '3');






