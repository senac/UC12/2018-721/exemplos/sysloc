-- Liste todos os gêneros
SELECT * FROM GENERO ;
-- Liste todos os atores
SELECT * FROM ATOR ;
-- Liste todos os filmes ordenados pelo ano
SELECT * FROM FILME ORDER BY ANO_LANCAMENTO ASC;
SELECT * FROM FILME ORDER BY ANO_LANCAMENTO DESC;
-- Liste todos os diretores
SELECT * FROM DIRETOR ;
-- Liste todos os filmes com o genero (id , titulo , genero , ano)
-- 1 - SELECT * FROM FILME ;
-- 2 - SELECT * FROM FILME INNER JOIN GENERO  ;
-- 3 - SELECT * FROM FILME INNER JOIN GENERO ON FILME.ID_GENERO = GENERO.ID ; 
-- 4 - SELECT * FROM FILME F INNER JOIN GENERO G ON F.ID_GENERO = G.ID ; 
SELECT F.ID , F.TITULO , G.NOME AS GENERO , F.ANO_LANCAMENTO AS ANO
FROM FILME F 
INNER JOIN GENERO G   ON ( F.ID_GENERO = G.ID ) ;


-- Liste todos os filmes do genero drama
SELECT  F.ID , F.TITULO , F.ANO_LANCAMENTO , F.AVALIACAO, 
F.TEMPO_DURACAO , G.NOME AS GENERO 
FROM FILME F  
INNER JOIN GENERO G ON (F.ID_GENERO = G.ID)
WHERE UPPER(G.NOME ) = UPPER('dRAMA') ;
-- Liste a quantidade de filmes por genero (genero , quantidade)
SELECT G.NOME AS GENERO , COUNT(1) AS 'Quantidade de filmes'
FROM FILME F 
INNER JOIN GENERO G 
ON F.ID_GENERO = G.ID
GROUP BY G.NOME
 ;


-- Liste os diretores com a quantidade de filmes que participou (nome , quantidade)
SELECT 
D.ID , 
CONCAT( D.PRIMEIRO_NOME  , ' ' , D.ULTIMO_NOME) AS DIRETOR ,
COUNT(1) AS 'Participações em filmes'
FROM DIRETOR D
INNER JOIN FILME F 
ON D.ID = F.ID_DIRETOR 
GROUP BY D.ID
;

SELECT 
D.ID , 
CONCAT( D.PRIMEIRO_NOME  , ' ' , D.ULTIMO_NOME) AS DIRETOR ,
COUNT(1) AS 'Participações em filmes'
FROM FILME F 
INNER JOIN DIRETOR D
ON F.ID_DIRETOR  = D.ID 
GROUP BY D.ID
;



-- Liste os filmes com a quantidade de atores ordenado pela 
-- quantidade de atores descendente (id , titulo , quantidade de atores)

SELECT F.ID , F.TITULO , COUNT(1) AS ATORES 
FROM FILME F 
INNER JOIN filme_ator FA ON (F.ID = FA.ID_FILME)
INNER JOIN ATOR A ON ( FA.ID_ATOR = A.ID)
GROUP BY F.ID 
ORDER BY ATORES DESC; 

-- Liste o filme mais antigo

SELECT * FROM FILME 
WHERE ANO_LANCAMENTO = (SELECT MIN(ANO_LANCAMENTO) FROM FILME)  ; 

-- SELECT * FROM FILME ORDER BY ANO_LANCAMENTO ASC LIMIT 5;



-- Liste o ator que mais teve participações em filmes

SELECT 
CONCAT (A.PRIMEIRO_NOME  , ' ' , A.ULTIMO_NOME ) AS ATOR  ,
COUNT(1) AS 'Participacoes'
FROM ATOR A 
INNER JOIN FILME_ATOR FA ON ( A.ID = FA.ID_ATOR ) 
INNER JOIN FILME F ON ( FA.ID_FILME = F.ID ) 
GROUP BY A.ID  HAVING Participacoes = (
											SELECT COUNT(1) FROM ATOR A 
											INNER JOIN FILME_ATOR FA ON ( A.ID = FA.ID_ATOR ) 
											INNER JOIN FILME F ON ( FA.ID_FILME = F.ID ) 
											GROUP BY A.ID ORDER BY 1 DESC LIMIT 1
									   ) ;
							












Liste o diretor que mais teve participações em filmes

Liste os filmes ordenados pelas avaliações

-- Liste o atores junto com a quantidade de participações em filmes

-- liste o ator que mais participou de filmes















