CREATE SCHEMA IF NOT EXISTS SYSLOC ;
USE SYSLOC ; 

CREATE TABLE IF NOT EXISTS Genero(
ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY  , 
NOME VARCHAR(200) NOT NULL 
);

CREATE TABLE IF NOT EXISTS Ator(
ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY  , 
Primeiro_Nome VARCHAR(200) NOT NULL , 
Ultimo_Nome VARCHAR(200) NOT NULL , 
Local_Nascimento VARCHAR(200) NOT NULL , 
Aniversario DATE NOT NULL 
);

CREATE TABLE IF NOT EXISTS Diretor(
ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY  , 
Primeiro_Nome VARCHAR(200) NOT NULL , 
Ultimo_Nome VARCHAR(200) NOT NULL , 
Local_Nascimento VARCHAR(200) NOT NULL , 
Aniversario DATE NOT NULL 
);

CREATE TABLE IF NOT EXISTS Filme(
ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY  , 
Titulo VARCHAR(1000) NOT NULL , 
ANO_LANCAMENTO INT NOT NULL , 
AVALIACAO INT NOT NULL , 
TEMPO_DURACAO INT NOT NULL ,
ID_GENERO INT NOT NULL  ,
ID_DIRETOR INT NOT NULL , 
FOREIGN KEY (ID_GENERO) REFERENCES GENERO(ID) ,
FOREIGN KEY (ID_DIRETOR) REFERENCES DIRETOR(ID) 
);


CREATE TABLE IF NOT EXISTS FILME_ATOR(
ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY  , 
ID_FILME INT NOT NULL , 
ID_ATOR INT NOT NULL ,
FOREIGN KEY (ID_FILME) REFERENCES FILME(ID) , 
Foreign key(ID_ATOR) references ATOR(id)

);


DROP SCHEMA SYSLOC ; 
